const commando = require('discord.js-commando');

class rpsCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'rps',
            group: 'rps',
            memberName: 'rps',
            description: 'Plays Archer Swordsman catapult'
        });

    }
    async run(message, args){
        var rand=Math.floor(Math.random()*3+1);
        var choice=0;
        if(message.content=='!rps archer'||message.content=='!rps Archer'){
            choice=1;
        }else if(message.content=='!rps catapult'||message.content=='!rps Catapult'){
            choice=2;
        }else if(message.content=='!rps swordsman'||message.content=='!rps Swordsman'){
            choice=3;
        }else{
            message.channel.send('enter !rps Archer/Swordsman/catapult');
        }
        if(rand==1&&choice==1){
            message.channel.send('JRR chose Archer');
            message.channel.send('It\'s a  tie');
        }else if(rand==1&&choice==2){
            message.channel.send('JRR chose Archer');
            message.channel.send('JRR Loses');
        }else if(rand==1&&choice==3){
            message.channel.send('JRR chose Archer');
            message.channel.send('JRR Wins');
        }else if(rand==2&&choice==1){
            message.channel.send('JRR chose catapult');
            message.channel.send('JRR Wins');
        }else if(rand==2&&choice==2){
            message.channel.send('JRR chose catapult');
            message.channel.send('It\'s a  tie');
        }else if(rand==2&&choice==3){
            message.channel.send('JRR chose catapult');
            message.channel.send('JRR Loses');
        }else if(rand==3&&choice==1){
            message.channel.send('JRR chose Swordsman');
            message.channel.send('JRR Loses');
        }else if(rand==3&&choice==2){
            message.channel.send('JRR chose Swordsman');
            message.channel.send('JRR Wins');
        }else if(rand==3&&choice==3){
            message.channel.send('JRR chose Swordsman');
            message.channel.send('It\'s a  tie');
        }
    }
}
module.exports = rpsCommand;