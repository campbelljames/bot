const commando = require('discord.js-commando');
const tu='👍';

class VoteunmuteCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'voteunmute',
            group: 'voteunmute',
            memberName: 'voteunmute',
            description: 'vote unmutes a member'
        });

    }
    async run(message, args){
        let unmutedUser = message.guild.member(message.mentions.users.first());
        if(!unmutedUser){
            message.channel.send("Sorry, I couldn't find that user");
            return;
        }else{
            let msg=await message.channel.send(unmutedUser+' is up for a vote if this message gets 4 upvotes the person will be unmuted')
            await msg.react(tu);
            const reaction = await msg.awaitReactions(reaction => reaction.emoji.name==tu,{time: 30000});
            if(reaction.get(tu).count-1>=4){
                message.guild.member(unmutedUser).serverMute('You were unmuted').then(console.log).catch(console.error);
                return;
            }else{
                message.channel.send(unmutedUser+" was not unmuted");
                return;
            }
        }

    }

}

module.exports = VoteunmuteCommand;