const commando = require('discord.js-commando');
const YTDL = require("ytdl-core");

function Play(connection,message){
    var server = servers[message.guild.id];
    server.dispatcher = connection.playStream(YTDL(server.queue[0],{filter: "audioonly"}));
    server.queue.shift();
    server.dispatcher.on('end',function(){
        if(server.queue[0]){
            Play(connection,message);
        }else{
            connection.disconnect();
        }
    });
}
class playCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'play',
            group: 'join',
            memberName: 'play',
            description: 'play anything in a channel with link because im not cool enough to get it without a link'
        });

    }
    async run(message, args){
        if(message.member.voiceChannel){
            if(!message.guild.voiceConnection){
                if(!servers[message.guild.id]){
                    servers[message.guild.id] = {queue:[]}
                }
                message.member.voiceChannel.join()
                .then(connection =>{
                    var server = servers[message.guild.id];
                    message.reply('played');
                    server.queue.push(args);
                    Play(connection,message);
                })
            }
        }
    }
}
module.exports = playCommand;