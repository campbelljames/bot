const commando = require('discord.js-commando');

class leaveCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'leave',
            group: 'join',
            memberName: 'leave',
            description: 'leaves Channel'
        });

    }
    async run(message, args){
        if(message.guild.voiceConnection){
            message.guild.voiceConnection.disconnect();
        }
    }
}
module.exports = leaveCommand;