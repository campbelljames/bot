const commando = require('discord.js-commando');
const Discord = require('discord.js');


class gifsCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'gifs',
            group: 'gifs',
            memberName: 'gifs',
            description: 'gifs from JRR'
        });

    }
    async run(message, args){
        var random = Math.floor(Math.random()*23+1);
        if(message.content=='!gifs commands'){
            message.channel.send('you shall not pass\nscary bilbo\nfree\nfly\ntators\npotatoes\nsmeagol scream\nmerry\nno friends\nnot listening\n stupid\nleave now\nlong shanks\n gandalf\n one does not simply\nmotivation\nelves\ndont need you\nsecond breakfast\nweird\nfrodo froth\ncreepy frodo\nfrodo fall\nrandom');
        }else if (message.content=='!gifs you shall not pass'){
            message.channel.send({files: ["https://media.giphy.com/media/3jviEqq1NLYcM/giphy.gif"]});
        }else  if (message.content=='!gifs scary bilbo'){
            message.channel.send({files: ["https://media.giphy.com/media/hcDd8ykjFXeyQ/giphy.gif"]});
        }else  if (message.content=='!gifs fly'){
            message.channel.send({files: ["https://media.giphy.com/media/ONostKY8aj8bK/giphy.gif"]});
        }else  if (message.content=='!gifs free'){
            message.channel.send({files: ["http://i.imgur.com/5B887tU.gif"]});
        }else  if (message.content=='!gifs tators'){
            message.channel.send({files: ["https://media.giphy.com/media/snML78KQpHLJC/giphy.gif"]});
        }else  if (message.content=='!gifs potatoes'){
            message.channel.send({files: ["https://i.imgur.com/mGKIRzn.gif"]});
        }else  if (message.content=='!gifs smeagol scream'){
            message.channel.send({files: ["http://www.reactiongifs.com/r/tumblr_mazjy9e9Dg1rvbizho2_250.gif"]});
        }else  if (message.content=='!gifs merry'){
            message.channel.send({files: ["http://media.tumblr.com/9a3c33f7ee40ac3da6d1f2d67cafc3a3/tumblr_inline_mgqe3yJZMY1qjg4p3.gif"]});
        }else  if (message.content=='!gifs no friends'){
            message.channel.send({files: ["https://78.media.tumblr.com/dedae984489a24466c5c7701abd4e115/tumblr_n6h1axzZb51td0dkco1_500.gif"]});
        }else  if (message.content=='!gifs not listening'){
            message.channel.send({files: ["https://i.imgur.com/YvJB6ot.gif"]});
        }else  if (message.content=='!gifs stupid'){
            message.channel.send({files: ["https://i.pinimg.com/originals/69/2d/25/692d25734538b707bb7a0a15250c5361.gif"]});
        }else  if (message.content=='!gifs leave now'){
            message.channel.send({files: ["https://media.giphy.com/media/7pBQGonWf0xJC/giphy.gif"]});
        }else  if (message.content=='!gifs long shanks'){
            message.channel.send({files: ["https://66.media.tumblr.com/tumblr_ltv4a83WWw1qek28no1_500.gif"]});
        }else  if (message.content=='!gifs gandalf'){
            message.channel.send({files: ["https://i.imgur.com/FH1kAfv.gif"]});
        }else  if (message.content=='!gifs one does not simply'){
            message.channel.send({files: ["https://data.whicdn.com/images/239429824/original.gif"]});
        }else  if (message.content=='!gifs motivation'){
            message.channel.send({files: ["http://images6.fanpop.com/image/photos/39200000/lotr-gifs-lord-of-the-rings-39229843-500-198.gif"]});
        }else  if (message.content=='!gifs elves'){
            message.channel.send({files: ["https://i.pinimg.com/originals/5b/d3/49/5bd349689fd053a83539a1987a499c8a.gif"]});
        }else  if (message.content=='!gifs dont need you'){
            message.channel.send({files: ["http://78.media.tumblr.com/tumblr_mddwm2or091qgzdhjo12_r1_250.gif"]});
        }else  if (message.content=='!gifs second breakfast'){
            message.channel.send({files: ["https://media3.giphy.com/media/UAYp3SqXKy7VC/200w.gif"]});
        }else  if (message.content=='!gifs weird'){
            message.channel.send({files: ["https://www.benedikt-grosser.com/files/reaction-gifs/lotr-gifs/councilofelrond0sj.gif"]});
        }else  if (message.content=='!gifs frodo froth'){
            message.channel.send({files: ["https://i.imgur.com/JxlwKNE.gif"]});
        }else  if (message.content=='!gifs creepy frodo'){
            message.channel.send({files: ["https://66.media.tumblr.com/538723632fa3c553f3bbf0cc9839eb8c/tumblr_inline_p7gq6k8fwr1sqa7cb_500.gif"]});
        }else  if (message.content=='!gifs frodo fall'){
            message.channel.send({files: ["https://media.giphy.com/media/3qb6Oxhw68HPq/giphy.gif"]});
        }else  if (message.content=='!gifs random'){
            if (random==1){
            message.channel.send({files: ["https://media.giphy.com/media/3jviEqq1NLYcM/giphy.gif"]});
            }else if (random==2){
            message.channel.send({files:["https://media.giphy.com/media/hcDd8ykjFXeyQ/giphy.gif"]});
            }else if(random==3){
            message.channel.send({files:["https://media.giphy.com/media/ONostKY8aj8bK/giphy.gif"]});
            }else if(random==4){
            message.channel.send({files:["http://i.imgur.com/5B887tU.gif"]});
            }else if(random==5){
            message.channel.send({files:["https://media.giphy.com/media/snML78KQpHLJC/giphy.gif"]});
            }else if(random==6){
            message.channel.send({files:["https://i.imgur.com/mGKIRzn.gif"]});
            }else if(random==7){
            message.channel.send({files:["http://www.reactiongifs.com/r/tumblr_mazjy9e9Dg1rvbizho2_250.gif"]});
            }else if(random==8){
            message.channel.send({files:["http://media.tumblr.com/9a3c33f7ee40ac3da6d1f2d67cafc3a3/tumblr_inline_mgqe3yJZMY1qjg4p3.gif"]});
            }else if(random==9){
            message.channel.send({files:["https://78.media.tumblr.com/dedae984489a24466c5c7701abd4e115/tumblr_n6h1axzZb51td0dkco1_500.gif"]});
            }else if(random==10){
            message.channel.send({files:["https://i.imgur.com/YvJB6ot.gif"]});
            }else if(random==11){
            message.channel.send({files:["https://i.pinimg.com/originals/69/2d/25/692d25734538b707bb7a0a15250c5361.gif"]});
            }else if(random==12){
            message.channel.send({files:["https://media.giphy.com/media/7pBQGonWf0xJC/giphy.gif"]});
            }else if(random==13){
            message.channel.send({files:["https://66.media.tumblr.com/tumblr_ltv4a83WWw1qek28no1_500.gif"]});
            }else if(random==14){
            message.channel.send({files:["https://i.imgur.com/FH1kAfv.gif"]});
            }else if(random==15){
            message.channel.send({files:["https://data.whicdn.com/images/239429824/original.gif"]});
            }else if(random==16){
            message.channel.send({files:["http://images6.fanpop.com/image/photos/39200000/lotr-gifs-lord-of-the-rings-39229843-500-198.gif"]});
            }else if(random==17){
                message.channel.send({files:["https://i.pinimg.com/originals/5b/d3/49/5bd349689fd053a83539a1987a499c8a.gif"]});
            }else if(random==18){
                message.channel.send({files:["http://78.media.tumblr.com/tumblr_mddwm2or091qgzdhjo12_r1_250.gif"]});
            }else if(random==19){
                message.channel.send({files:["https://media3.giphy.com/media/UAYp3SqXKy7VC/200w.gif"]});
            }else if(random==20){
                message.channel.send({files:["https://www.benedikt-grosser.com/files/reaction-gifs/lotr-gifs/councilofelrond0sj.gif"]});
            }else if(random==21){
                message.channel.send({files:["https://i.imgur.com/JxlwKNE.gif"]});
            }else if(random==22){
                message.channel.send({files:["https://66.media.tumblr.com/538723632fa3c553f3bbf0cc9839eb8c/tumblr_inline_p7gq6k8fwr1sqa7cb_500.gif"]});
            }else{
                message.channel.send({files:["https://media.giphy.com/media/3qb6Oxhw68HPq/giphy.gif"]});
            }
        }else{
            message.channel.send('Type !gifs commands');
        }
    }

 
}

module.exports = gifsCommand;