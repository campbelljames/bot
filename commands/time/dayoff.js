const commando = require('discord.js-commando');

class timeCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'dayoff',
            group: 'time',
            memberName: 'dayoff',
            description: 'time to the next day off'
        });

    }
    async run(message, args){
        var countDownDate = new Date("Nov 6, 2018 00:00:00").getTime();
            var now = new Date().getTime(); // Find the distance between now and the count down date 
            var distance = countDownDate - now; // Time calculations for days, hours, minutes and seconds
            var day='Nov 6';
            var cool = true;
            if(distance<=0){
                countDownDate = new Date("Sep 30, 2019 00:00:00").getTime();
                now = new Date().getTime();
                distance=countDownDate - now;
                day='Sep 30';
                if(distance<=0){
                    countDownDate = new Date("Oct 9, 2019 00:00:00").getTime();
                    now = new Date().getTime();
                    distance=countDownDate - now;
                    day='Oct 9';
                    if(distance<=0){
                        countDownDate = new Date("Oct 18, 2019 00:00:00").getTime();
                        now = new Date().getTime();
                        distance=countDownDate - now;
                        day='Oct 18';
                        if(distance<=0){
                            countDownDate = new Date("Nov 27, 2019 00:00:00").getTime();
                            now = new Date().getTime();
                            distance=countDownDate - now;
                            day='Nov 27';
                            if(distance<=0){
                                countDownDate = new Date("Dec 23, 2019 00:00:00").getTime();
                                now = new Date().getTime();
                                distance=countDownDate - now;
                                day='Dec 23';
                                if(distance<=0){
                                    countDownDate = new Date("Jan 20, 2020 00:00:00").getTime();
                                    now = new Date().getTime();
                                    distance=countDownDate - now;
                                    day='Jan 20';
                                    if(distance<=0){
                                        countDownDate = new Date("Jan 27, 2020 00:00:00").getTime();
                                        now = new Date().getTime();
                                        distance=countDownDate - now;
                                        day='Jan 27';
                                        if(distance<=0){
                                            countDownDate = new Date("Feb 17, 2020 00:00:00").getTime();
                                            now = new Date().getTime();
                                            distance=countDownDate - now;
                                            day='Feb 17';
                                            if(distance<=0){
                                                countDownDate = new Date("Apr 6, 2020 00:00:00").getTime();
                                                now = new Date().getTime();
                                                distance=countDownDate - now;
                                                day='Apr 6';
                                                if(distance<=0){
                                                    countDownDate = new Date("Apr 28, 2020 00:00:00").getTime();
                                                    now = new Date().getTime();
                                                    distance=countDownDate - now;
                                                    day='Apr 28';
                                                    if(distance<=0){
                                                        countDownDate = new Date("May 25, 2020 00:00:00").getTime();
                                                        now = new Date().getTime();
                                                        distance=countDownDate - now;
                                                        day='May 25';
                                
                                                        if(distance<=0){
                                                        cool = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            var days = Math.floor(distance / (1000 * 60 * 60 * 24)); 
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)); 
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)); 
            var seconds = Math.floor((distance % (1000 * 60)) / 1000); 
            if(cool){
            return message.channel.send(days+" days "+hours+" hours "+minutes+" minutes "+seconds+" seconds until the next day off on "+day);
            }else{
                message.channel.send("There are no more days off!");
            }
            }
}
module.exports = timeCommand;