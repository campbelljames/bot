const commando = require('discord.js-commando');

class timeCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'time',
            group: 'time',
            memberName: 'time',
            description: 'time to the end of school'
        });

    }
    async run(message, args){
        var countDownDate = new Date("Jun 21, 2020 14:10:00").getTime();
            var now = new Date().getTime(); // Find the distance between now and the count down date 
            var distance = countDownDate - now; // Time calculations for days, hours, minutes and seconds 
            var days = Math.floor(distance / (1000 * 60 * 60 * 24)); 
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)); 
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)); 
            var seconds = Math.floor((distance % (1000 * 60)) / 1000); 
            return message.channel.send(days+" days "+hours+" hours "+minutes+" minutes "+seconds+" seconds until the end of school");
            }
}
module.exports = timeCommand;
