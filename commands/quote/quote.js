const commando = require('discord.js-commando');
const Discord = require('discord.js');


class QuoteCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'quote',
            group: 'quote',
            memberName: 'quote',
            description: 'quotes JRR'
        });

    }
    async run(message, args){
        var random = Math.floor(Math.random()*8+1);

        if(random==1){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("One ring to rule them all, one ring to find them, One ring to bring them all and in the darkness bind them.", "-Gandalf");
            message.channel.send(textEmbed);
            return;
        }else if(random==2){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("A wizard is never late, Frodo Baggins. Nor is he early. He arrives precisely when he means to.", "-Gandalf");
            message.channel.send(textEmbed);
            return;
        }else if(random==3){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("YOU SHALL NOT PASS!", "-Gandalf");
            message.channel.send(textEmbed);
            return;
}else if(random==4){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("It’s a dangerous business, Frodo, going out your door…You step into the Road, and if you don’t keep your feet, there is no knowing where you might be swept off to.", "-Bilbo Baggins");
            message.channel.send(textEmbed);
            return;
        }else if(random==5){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("It’s the job that’s never started as takes longest to finish.", "-Galadrial");
            message.channel.send(textEmbed);
            return;
        }else if(random==6){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("What do you fear, lady?’ he asked. ‘A cage,’ she said, ‘To stay behind bars, until use and old age accept them, and all chance of doing great deeds is gone beyond recall or desire.", "-Aragorn");
            message.channel.send(textEmbed);
            return;
        }else if(random==7){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("I would have followed you, my brother, my captain, my king.", "-Boramir");
            message.channel.send(textEmbed);
            return;
        }else if(random==8){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("Faithless is he that says farewell when the road darkens.", "-Gimli");
            message.channel.send(textEmbed);
            return;
        }else if(random==9){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("I will not say, do not weep, for not all tears are an evil", "-Gandalf");
            message.channel.send(textEmbed);
            return;
        }else if(random==10){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("All we have to decide is what to do with the time that is given us.", "-Gandalf");
            message.channel.send(textEmbed);
            return;
        }else if(random==11){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("The Road goes ever on and on\nDown from the door where it began.\nNow far ahead the Road has gone,\nAnd I must follow, if I can.", "-JRR Tolkien");
            message.channel.send(textEmbed);
            return;
        }
        else if(random==12){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("If more of us valued food and cheer and song above hoarded gold, it would be a merrier world.", "-Thorin");
            message.channel.send(textEmbed);
            return;
        }
        else if(random==12){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("The world is indeed full of peril, and in it there are many dark places; but still there is much that is fair, and though in all lands love is now mingled with grief, it grows perhaps the greater", "-JRR Tolkien");
            message.channel.send(textEmbed);
            return;
        }
        else if(random==13){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("A man that flies from his fear may find that he has only taken a short cut to meet it.", "-JRR Tolkien");
            message.channel.send(textEmbed);
            return;
        }        
        else if(random==14){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("No half-heartedness and no worldly fear must turn us aside from following the light unflinchingly.", "-JRR Tolkien");
            message.channel.send(textEmbed);
            return;
        }
        else if(random==15){
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("Short cuts make long delays.", "-JRR Tolkien");
            message.channel.send(textEmbed);
            return;
        }
        
        else{
            let textEmbed = new Discord.RichEmbed()
            .setColor("#e56b00")
            .addField("Even the smallest person can change the course of the future.", "-Galadriel");
            message.channel.send(textEmbed);
            return;
        }


    }

}

module.exports = QuoteCommand;