const commando = require('discord.js-commando');
const tu='👍';

class VoteKickCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'votekick',
            group: 'votekick',
            memberName: 'votekick',
            description: 'votekicks a member'
        });

    }
    async run(message, args){
        let kickedUser = message.guild.member(message.mentions.users.first());
        if(!kickedUser){
            message.channel.send("Sorry, I couldn't find that user",{files: ["https://i.imgur.com/ofWuq.jpg"]});
            return;
        }else{
            let msg=await message.channel.send(kickedUser+' is up for a vote if this message gets 6 upvotes the person will be kicked')
            await msg.react(tu);
            const reaction = await msg.awaitReactions(reaction => reaction.emoji.name==tu,{time: 30000});
            if(reaction.get(tu).count-1>=6){
                message.guild.member(kickedUser).kick('You were voted off').then(console.log).catch(console.error);
                return;
            }else{
                message.channel.send(kickedUser+" was not kicked");
                return;
            }
        }

    }

}

module.exports = VoteKickCommand;