const commando = require('discord.js-commando');

class dicerollCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'diceroll',
            group: 'diceroll',
            memberName: 'diceroll',
            description: 'Rolls a die'
        });

    }
    async run(message, args){
        var rand=Math.floor(Math.random()*6+1);
        message.channel.send('You rolled a '+rand);
    }
}
module.exports = dicerollCommand;