const commando = require('discord.js-commando');

class jokeCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'joke',
            group: 'joke',
            memberName: 'joke',
            description: 'Tells a joke'
        });

    }
    async run(message, args){
        var rand=Math.floor(Math.random()*12+1);
        if(rand==1){
            message.channel.send('What do you call a hobbit party?\n A little get together.');
        }else if(rand==2){
            message.channel.send('Why don\'t you ask a hobbit for money?\n Because they\'re always a little short.');
        }else if(rand==3){
            message.channel.send('What do you call a gangster hobbit? \nYolo Swaggins.');
        }else if(rand==4){
            message.channel.send('Why can\'t you enter Sauron\'s lair?\n Because there\'s always one Mordor.');
        }else if(rand==5){
            message.channel.send('Why did the Best Man go to Mount Doom?\nBecause he was the Ring-bearer!');
        }else if(rand==6){
            message.channel.send('Why shouldn\'t you piss off a dwarf?\nBecause he\'s got a short temper.');
        }else if(rand==7){
            message.channel.send('What is Gollum\'s favorite bird?\n A Smeagull!');
        }else if(rand==8){
            message.channel.send('How did the hobbit ruin the boxing match?\nHe tried to destroy the ring!'); 
        }else if(rand==9){
            message.channel.send('What did the guy say when he bumped into the wizard?\nSaruman, I didn’t see you there.');
        }else if(rand==10){
            message.channel.send('What did Frodo say when he saw the trees dancing?\nThat\’s ENT-tertainment!');
        }else if(rand==11){
            message.channel.send('Why are most hobbits good guys?\nBecause they don\'t look down on people.');
        }else if(rand==12){
            message.channel.send('Yo mama so ugly she has the breath of an Orc, the face of a Dwarf, and the feet of a Hobbit.');
        }
    }
}
module.exports = jokeCommand;