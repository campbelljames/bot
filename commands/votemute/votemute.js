const commando = require('discord.js-commando');
const tu='👍';

class VotemuteCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'votemute',
            group: 'votemute',
            memberName: 'votemute',
            description: 'vote mutes a member'
        });

    }
    async run(message, args){
        const muteRole = message.guild.roles.find("name", "Muted");
        let mutedUser = message.guild.member(message.mentions.users.first());
        if(!mutedUser){
            message.channel.send("Sorry, I couldn't find that user");
            return;
        }else{
            let msg=await message.channel.send(mutedUser+' is up for a vote if this message gets 4 upvotes the person will be muted')
            await msg.react(tu);
            const reaction = await msg.awaitReactions(reaction => reaction.emoji.name==tu,{time: 30000});
            if(reaction.get(tu).count==4){
                client.muteMember(mutedUser).catch(console.error); 
                message.channel.send(mutedUser+' was muted');
                return;
            }else{
                message.channel.send(mutedUser+" was not muted");
                return;
            }
        }

    }

}

module.exports = VotemuteCommand;