const commando = require('discord.js-commando');

class pickuplineCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'pickupline',
            group: 'pickupline',
            memberName: 'pickupline',
            description: 'JRR says a pick up line'
        });

    }
    async run(message, args){
        var rand=Math.floor(Math.random()*31+1);
        if(rand==1){
            message.channel.send('The fires of Mount Doom aren’t nearly as hot as you are.');
        }else if(rand==2){
            message.channel.send('Are you Gandalf? Because you have me seeing fireworks.');
        }else if(rand==3){
            message.channel.send('Was your face forged by Sauron? Cause you’re precious');
        }else if(rand==4){
            message.channel.send('Speak, friend, and enter… your number into my phone.');
        }else if(rand==5){
            message.channel.send('Excuse me, are you the Arkenstone? Because you look like something I could treasure.');
        }else if(rand==6){
            message.channel.send('You shall not pass… without giving me those digits.');
        }else if(rand==7){
            message.channel.send('You must be Frodo Baggins, because I\'m the One and you’ve got me wrapped around your finger');
        }else if(rand==8){
            message.channel.send('Watch out–I\'m about to rescue you from Lonely Mountain.'); 
        }else if(rand==9){
            message.channel.send('Well, Sauron’s gonna be after me now–I think I’ve found the One');
        }else if(rand==10){
            message.channel.send('Are there Orcs nearby? Because you’re simply glowing tonight.');
        }else if(rand==11){
            message.channel.send('My feet are as big as a hobbit’s, and you know what they say about big feet.');
        }else if(rand==12){
            message.channel.send('I’m looking for someone to share in an adventure.');
        }else if(rand==13){
            message.channel.send('A wizard is never premature. He finishes precisely when he means to.');
        }else if(rand==14){
            message.channel.send('Are you an orc? Because you’re making my Sting glow.');
        }else if(rand==15){
            message.channel.send('Are you the Ring? \'Cause I\'ve got my eye on you.');
        }else if(rand==16){
            message.channel.send('Baby, I\'ll make you scream like the Nazgul.');
        }else if(rand==17){
            message.channel.send('Beard can be red; A blade can glow blue; There is only one precious, and that must be you.');
        }else if(rand==18){
            message.channel.send('Did it hurt when you fell from the Timeless Halls?');
        }else if(rand==19){
            message.channel.send('Gimli a piece of that!');
        }else if(rand==20){
            message.channel.send('Girl, you\'re like a Balrog - smokin\' hot!');
        }else if(rand==21){
            message.channel.send('Hey baby, wanna blow my Horn of Gondor?');
        }else if(rand==22){
            message.channel.send('I last longer than Boromir.');
        }else if(rand==23){
            message.channel.send('I erupt like Mount Doom for you, baby!');
        }else if(rand==24){
            message.channel.send('I\'d like to have an Inn at your prancing pony.');
        }else if(rand==25){
            message.channel.send('I\'ll take your Hobbit to my Isengard.');
        }else if(rand==26){
            message.channel.send('I\'ll have you for second-breakfast.');
        }else if(rand==27){
            message.channel.send('I\'ve got a feeling you’re about to become my Precious.');
        }else if(rand==28){
            message.channel.send('Is your name Smaug? Because you look like you have loads of treasure in your cave.');
        }else if(rand==29){
            message.channel.send('My love is like Lembas bread, one taste to get your fill.');
        }else if(rand==30){
            message.channel.send('That aromatic scent is my manflesh.');
        }else if(rand==31){
            message.channel.send('Your beauty has pierced my heart like a Morgul Blade.');
        }
    }
}
module.exports = pickuplineCommand;