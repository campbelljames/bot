const commando = require('discord.js-commando');
const YTDL = require("ytdl-core");

function Play(connection,message){
    var server = servers[message.guild.id];
    if(message.content=='!playlotr black gates'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=EXGUNvIFTQw',{filter: "audioonly"}));
    }else if(message.content=='!playlotr shire'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=LML6SoNE7xE',{filter: "audioonly"}));
    }else if(message.content=='!playlotr minas morgul'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=5H5Cq4_gov8',{filter: "audioonly"}));
    }else if(message.content=='!playlotr rohan'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=fxfaQrS1KIs',{filter: "audioonly"}));
    }else if(message.content=='!playlotr smeagol monologue'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=EbzFmkHEv1Q',{filter: "audioonly"}));
    }else if(message.content=='!playlotr boromir death'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=gyHl4l0zL-k',{filter: "audioonly"}));
    }else if(message.content=='!playlotr frodo leaves'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=rCY_Hjv7vKc&t=134s',{filter: "audioonly"}));
    }else if(message.content=='!playlotr theoden speech'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=POdknqszMDY',{filter: "audioonly"}));
    }else if(message.content=='!playlotr i am no man'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=aNL9oljAFqM',{filter: "audioonly"}));
    }else if(message.content=='!playlotr helms deep'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=05XBQv0dUkA',{filter: "audioonly"}));
    }else if(message.content=='!playlotr moria'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=zBoGSMuuHf4',{filter: "audioonly"}));
    }else if(message.content=='!playlotr nazgul theme'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=2DFYvwn_xLM',{filter: "audioonly"}));
    }else if(message.content=='!playlotr rivendell'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=I5grxiqabIg',{filter: "audioonly"}));
    }else if(message.content=='!playlotr lothlorien'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=RCVG9SeMqHA',{filter: "audioonly"}));
    }else if(message.content=='!playlotr full soundtrack'){
        server.dispatcher = connection.playStream(YTDL('https://www.youtube.com/watch?v=_SBQvd6vY9s',{filter: "audioonly"}));
    }else{
        message.channel.send('type !playlotr with any of these commands:\nblack gates\nshire\nminan morgul\nrohan\nsmeagol monologue\nboromir death\nfrodo leaves\ntheoden speech\ni am no man\nhelms deep\nmoria\nnazgul theme\nrivendell\nlothlorien\nfull soundtrack');
    }
    server.queue.shift();
    server.dispatcher.on('end',function(){
        if(server.queue[0]){
            Play(connection,message);
        }else{
            connection.disconnect();
        }
    });
}
class playlotrCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'playlotr',
            group: 'playlotr',
            memberName: 'playlotr',
            description: 'play lotr in Channel'
        });

    }
    async run(message, args){
        if(message.member.voiceChannel){
            if(!message.guild.voiceConnection){
                if(!servers[message.guild.id]){
                    servers[message.guild.id] = {queue:[]}
                }
                message.member.voiceChannel.join()
                .then(connection =>{
                    var server = servers[message.guild.id];
                    message.reply('played');
                    server.queue.push(args);
                    Play(connection,message);
                })
            }
        }
    }
}
module.exports = playlotrCommand;