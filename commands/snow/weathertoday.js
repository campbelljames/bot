const commando = require('discord.js-commando');
const Discord = require('discord.js');
const weather=require('weather-js');
class weathertodayCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'weathertoday',
            group: 'weather',
            memberName: 'weathertoday',
            description: 'Tells the weather today'
        });

    }
    async run(message, args){
        weather.find({search: 'Elkridge, MD',degreeType:'F'},function(err,result){
            if(err) message.channel.send(err);
            var current=result[0].current;
            var location=result[0].location;
            const embed = new Discord.RichEmbed()
            .setDescription(current.skytext)
            .setAuthor('weather for '+current.observationpoint)
            .setThumbnail(current.imageUrl)
            .setColor(0x00AE86)
            .addField('Temperature',current.temperature+' Degrees',true)
            .addField('Feels Like',current.feelslike+' Degrees',true)
            .addField('Winds',current.winddisplay,true)
            .addField('Humidity',current.humidity+'%',true)
        message.channel.send({embed});
        });
    }
}
module.exports = weathertodayCommand;