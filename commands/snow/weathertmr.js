const commando = require('discord.js-commando');
const Discord = require('discord.js');
const weather=require('weather-js');
class weathertmrCommand extends commando.Command{
    constructor(client){
        super(client, {
            name: 'weathertmr',
            group: 'weather',
            memberName: 'weathertmr',
            description: 'Tells the weather tmr'
        });

    }
    async run(message, args){
        weather.find({search: 'Elkridge, MD',degreeType:'F'},function(err,result){
            if(err) message.channel.send(err);
            var forecast=result[0].forecast;
            var current=result[0].current;
            var location=result[0].location;
            const embed = new Discord.RichEmbed()
            .setDescription(forecast[0].skytext)
            .setAuthor('Weather for '+current.observationpoint)
            .setColor(0x00AE86)
            .addField('Temperature Low',forecast[0].low+' Degrees',true)
            .addField('Temperature High',forecast[0].high+' Degrees',true)
            .addField('Day',forecast[0].day,true)
            .addField('Precipitation',forecast[0].precip+'%',true)
        message.channel.send({embed});
        });
    }
}
module.exports = weathertmrCommand;